#ifndef __PCAP_TEST_H__
#define __PCAP_TEST_H__

#include <pcap/pcap.h>
#include <stdbool.h>

#define TIMEOUT_MS 10000

bool analyze (pcap_t *device, char const *amount);
bool configure_device (pcap_t *device, const char *dev_str);
void packet_received (u_char *args, const struct pcap_pkthdr *header, 
	const u_char *packet);
void send_packet (u_char *args, const struct pcap_pkthdr *header, 
	const u_char *packet);
pcap_t * startup_device(const char * dev_name);
void print_stats (pcap_t *device);
bool inject_packets(const char *argv[], pcap_t *dev);

#endif  //__PCAP_TEST_H__