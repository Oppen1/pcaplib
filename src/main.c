#include <string.h>

#include "main.h"
#include "pcap_test.h"

extern pcap_t *dev;

int main(int argc, char const *argv[])
{	
	if (!validate_args(argc, argv)) {
		return 1;
	}

	dev = startup_device(argv[2]);
	if (dev == NULL) {
		return 1;
	}

	if (argc == 4) {
		if (!analyze(dev, argv[3])) {
			// Close the opened device
			pcap_close(dev);
			return 1;
		};
		// Close the opened device
		pcap_close(dev);
	} else {
		// inyect packets from a file
		if (!inject_packets(argv, dev)) {
			return 1;
		}
	}
	return 0;
}

bool validate_args (int argc, char const *argv[]) {
	if (argc < 4) {
		fprintf(stderr, "Usage:\n\t ./pcap_test listen <interface> <amount of packets to sniff>\n");
		fprintf(stderr, "or \t ./pcap_test inject <interface> <number of iterations> <pcap_file>\n");
		return false;
	}
	if (strcmp(argv[1], "inject") != 0 && strcmp(argv[1], "listen") != 0) {
		fprintf(stderr, "Command not found. Available commands: listen or inject\n");
		return false;
	}

	if (argc == 4 && strcmp(argv[1], "listen") != 0) {
		fprintf(stderr, "Wrong number of arguments.\n");
		return false;
	}

	if (argc == 5 && strcmp(argv[1], "inject") != 0) {
		fprintf(stderr, "Wrong number of arguments.\n");
		return false;
	}
	return true;
}