#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdbool.h>

bool validate_args (int argc, char const *argv[]);

#endif  //__MAIN_H__