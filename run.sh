#!/bin/bash
if [ "$1" == "" ] || [ "$2" == "" ]; then
    echo "Usage: ./run.sh <amount of packets to send> <tcpreplay|auto> [pcap file]"
    exit 1
else
	echo "Creating network interfaces veth1 and veth2"
	sudo ip link add veth1 type veth peer name veth2
	echo "Setting up the interfaces"
	sudo ip link set dev veth1 up
	sudo ip link set dev veth2 up
	echo "Compiling"
	if [ ! -d "./build" ]; then
		mkdir build
	fi
	gcc -Wall -Werror -pedantic -o ./build/pcap_test ./src/main.c ./src/pcap_test.c -lpcap
	echo "sudo ./build/pcap_test listen veth2 $1 > stats.txt 2>stats.err&"
	sudo ./build/pcap_test listen veth2 $1 > stats.txt 2>stats.err & 
	sleep 1

	if [ "$3" != "" ]; then
		PCAP_FILE=$3
	else
		PCAP_FILE=singlex7262.pcap
	fi

	if [ "$2" == "auto" ]; then
		echo "sudo ./build/pcap_test inject veth1 $1 $PCAP_FILE > inject.txt 2>inject.err &"
		sudo ./build/pcap_test inject veth1 $1 $PCAP_FILE > inject.txt 2>inject.err &
	else 
		echo "sudo tcpreplay -i veth1 --loop=$1 $PCAP_FILE"
		sudo tcpreplay -i veth1 --loop=$1 $PCAP_FILE > temp.txt &
	fi
	echo "waiting"
	wait
	if [ "$2" != "auto" ]; then 
		rm temp.txt
	fi
	echo -e "execution finished \n"
	echo "grep veth2 /proc/net/dev | awk '{print \"|\"\$1 \"\t\t|\" \$3 \"\t\t|\" \$5  \"\t|\" }'"
	echo -e "|interface\t|packets recv\t|drops\t|"
	echo "-----------------------------------------"
	grep veth2 /proc/net/dev | awk '{print "|" $1 "\t\t|" $3 "\t\t|" $5  "\t|" }'
	echo -e "\ncat stats.txt"
	cat stats.txt
	echo "Deleting network interfaces"
	sudo ip link delete veth1
fi